package jp.masakura.featureflags.demo.controllers;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public final class UsernameAdvice {
    @ModelAttribute
    public void model(Model model, @AuthenticationPrincipal User user) {
        String username = user != null ? user.getUsername() : null;
        model.addAttribute("username", username);
    }
}
