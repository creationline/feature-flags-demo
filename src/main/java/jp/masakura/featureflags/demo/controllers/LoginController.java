package jp.masakura.featureflags.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("login")
public final class LoginController {
    @GetMapping
    public String get() {
        return "login/index";
    }

    @PostMapping
    public String postLogin() {
        return "redirect:/";
    }
}
