package jp.masakura.featureflags.demo.controllers;

import no.finn.unleash.Unleash;
import no.finn.unleash.UnleashContext;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public final class FeatureFlagsAdvice {
    private final Unleash unleash;

    public FeatureFlagsAdvice(Unleash unleash) {
        this.unleash = unleash;
    }

    @ModelAttribute
    public void model(Model model, @AuthenticationPrincipal User user) {
        String username = user != null ? user.getUsername() : null;

        UnleashContext.Builder builder = UnleashContext.builder();
        if (username != null) builder.userId(username);
        UnleashContext unleashContext = builder.build();

        model.addAttribute("surveyEmphasizeButton",
                unleash.isEnabled("survey-emphasize-button", unleashContext));

    }
}
