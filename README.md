# GitLab Feature Flags Demo
[GitLab Feature Flags](https://docs.gitlab.com/ee/operations/feature_flags.html) のデモ。


## GitLab Feature Flags とは?
GitLab プロジェクトから、アプリの外観や機能を簡単に切り替えられる機能です。

![](./docs/feature-flags.png)


## デモアプリ
アンケートの提出ボタンの見た目を Feature Flags で切り替えています。

* [development](https://gitlab-ff-demo-dev.azurewebsites.net/) - すべてのユーザーで `survey-emphasize-button` が有効です。
* [production](https://gitlab-ff-demo-prod.azurewebsites.net/) - User1 のみ `survey-emphasize-button` が有効です。右上のログインボタンからログインして、アンケートの提出ボタンがどうなるか見てみてください。


### 自分のパソコンで動かす
[feature-flags-demo-1.0-SNAPSHOT.war](https://gitlab.com/creationline/feature-flags-demo/-/jobs/artifacts/master/raw/build/libs/feature-flags-demo-1.0-SNAPSHOT.war?job=build) をダウンロードします。

[GitLab で新しいプロシェクトを作成](https://gitlab.com/projects/new#blank_project)し、プロジェクトのサイドメニューから `Deployments` -> `Feature Flags` を開いて、`New feature flag` をクリックして `survey-emphasize-button` を追加してください。

同じく Feature Flags ページの `Configure` ボタンをクリックして、`API URL` と `Instance ID` をメモします。

次のコマンドを実行します。

```shell
$ SPRING_PROFILES_ACTIVE=development GITLAB_FEATURE_FLAGS_API_URL=<API URL> GITLAB_FEATURE_FLAGS_INSTANCE_ID=<Instance ID> java -jar path/to/feature-flags-demo-1.0-SNAPSHOT.war --server.port=8080  
```

Windows のコマンドプロンプトでは次のようにします。

```shell
> set SPRING_PROFILES_ACTIVE=development
> GITLAB_FEATURE_FLAGS_API_URL=<API URL>
> GITLAB_FEATURE_FLAGS_INSTANCE_ID=<Instance ID>
> java -jar path\to\feature-flags-demo-1.0-SNAPSHOT.war --server.port=8080
```

ブラウザーを起動して、http://localhost:8080 にアクセスします。

GitLab Feature Flags の戦略を変更してアンケートページがどう変わるか確認してください。30 秒おきに Feature Flags を取得していますので、アンケートページの見た目が変わらないときは少し時間をおいてください。


## Contribute
おかしなところや変更してほしいところがあれば、[Issue を立てて](https://gitlab.com/creationline/feature-flags-demo/-/issues/new) ください。

デプロイ環境 (Azure Web Apps) の構築は [./ansible](./ansible) にあります。